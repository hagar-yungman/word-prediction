package com.countword;

        import com.amazonaws.regions.Regions;
        import com.amazonaws.services.s3.AmazonS3;
        import com.amazonaws.services.s3.AmazonS3ClientBuilder;
        import com.amazonaws.services.s3.model.*;
        import com.amazonaws.thirdparty.ion.IonException;
        import org.apache.hadoop.conf.Configuration;
        import org.apache.hadoop.filecache.DistributedCache;
        import org.apache.hadoop.fs.*;
        import org.apache.hadoop.fs.s3a.S3AFileSystem;
        import org.apache.hadoop.io.DoubleWritable;
        import org.apache.hadoop.io.LongWritable;
        import org.apache.hadoop.io.Text;
        import org.apache.hadoop.mapreduce.Job;
        import org.apache.hadoop.mapreduce.Mapper;
        import org.apache.hadoop.mapreduce.Partitioner;
        import org.apache.hadoop.mapreduce.Reducer;
        import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
        import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
        import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

        import java.io.*;
        import java.net.URI;
        import java.nio.charset.StandardCharsets;
        import java.util.HashMap;
        import java.util.Map;
        import java.util.TreeMap;

public class CalcProb2 {

    public static class MapClass extends Mapper<LongWritable, Text, Text, Text> {

        private static HashMap<String, Double> oneGramMap = new HashMap<String, Double>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {

            URI[] cacheFilesUri = Job.getInstance(context.getConfiguration()).getCacheFiles();
            System.out.println("test print in  setup");
            FileSystem fileSystem = FileSystem.get(context.getConfiguration());
//            if ((cacheFilesUri == null)||(cacheFilesUri.length == 0)){
//                System.out.println("cache file didn't work");
//                return;
//            }
//
            for (URI cacheFileUri : cacheFilesUri) {
                loadOneGramHashMap(cacheFileUri);
            }
        }

        private void loadOneGramHashMap(URI cacheFile) throws IOException, FileNotFoundException {

            String strLineRead;
            BufferedReader brReader = null;

            try {
                brReader = new BufferedReader(new InputStreamReader(new FileInputStream
                        (new Path(cacheFile.getPath()).getName()), StandardCharsets.UTF_8));
                while ((strLineRead = brReader.readLine()) != null) {
                    String word[] = strLineRead.split("\t");
                    oneGramMap.put(word[0], Double.valueOf(word[1]));
                }
            } finally {
                if (brReader != null) {
                    brReader.close();
                }
            }
        }

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] sentence = value.toString().replace("\t", " ").split(" ");
            double totalCorpus = oneGramMap.get("*");
            String w1w2w3 = sentence[0] + " " + sentence[1] + " " + sentence[2];
            double w1w2w3Value = Double.valueOf(sentence[3]);
            String w1w2 = sentence[4] + " " + sentence[5];
            double w1w2Value = Double.valueOf(sentence[6]);
            String w2w3 = sentence[7] + " " + sentence[8];
            double w2w3Value = Double.valueOf(sentence[9]);
            String w2 = sentence[1];
            String w3 = sentence[2];
            double w3value = oneGramMap.get(w3);
            double w2value = oneGramMap.get(w2);

            double k2 = (Math.log10(w2w3Value + 1) + 1) / (Math.log10(w2w3Value + 1) + 2);
            double k3 = (Math.log10(w1w2w3Value + 1) + 1) / (Math.log10(w1w2w3Value + 1) + 2);
            double prob = k3 * (w1w2w3Value / w1w2Value) +
                    (1 - k3) * k2 * (w2w3Value / w2value) +
                    (1 - k3) * (1 - k2) * (w3value / totalCorpus);
            String newKey = w1w2;
            String newValue = String.format("%s %s", w1w2w3, Double.toString(prob));
            context.write(new Text(newKey), new Text(newValue));
        }
    }

    public static class ReduceClass extends Reducer<Text, Text, Text, Text> {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            TreeMap<Double, String> sentenceProb = new TreeMap();
            for (Text value : values) {
                String[] sentence = value.toString().split(" ");
                sentenceProb.put(1 - Double.valueOf(sentence[3]), value.toString());
            }
            for (Map.Entry<Double, String> entry : sentenceProb.entrySet()) {
                String[] sentence = entry.getValue().split(" ");
                String prob = sentence[3];
                String words = String.format("%s %s %s", sentence[0], sentence[1], sentence[2]);
                context.write(new Text(words), new Text(prob));
            }
        }
    }

    public static class PartitionerClass extends Partitioner<Text, Text> {
        @Override
        public int getPartition(Text key, Text value, int numPartitions) {
            return Math.abs(key.hashCode()) % numPartitions;
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        Job job = new Job(conf, "prob1");
        job.setJarByClass(CalcProb2.class);
        job.setMapperClass(MapClass.class);
        job.setPartitionerClass(PartitionerClass.class);
        job.setReducerClass(ReduceClass.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);

        FileSystem fileSystem = new S3AFileSystem();
        //args[2] = bucket name
        fileSystem.initialize(URI.create(args[2]), conf);
        //args[3] = path to 1 gram output
        RemoteIterator<LocatedFileStatus> itr = fileSystem.listFiles(new Path(args[3]), false);
        while (itr.hasNext()){
            LocatedFileStatus file = itr.next();
            if(file.getPath().getName().equals("_SUCCESS")){
                continue;
            }
            System.out.println("uri: " + file.getPath().toUri());
            System.out.println("path name: " + file.getPath().getName());
            job.addCacheFile(file.getPath().toUri());
        }
        fileSystem.close();

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);


    }

}
