package com.countword;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.LongWritable;


public class Join1 {

    public static class MapClass2Gram extends Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String _value = value.toString().replace("\t", " ");
            String[] splitWordsBySpace = _value.split(" ");
            String _key = String.format("%s %s", splitWordsBySpace[0], splitWordsBySpace[1]);
            context.write(new Text(_key), new Text(_value));
        }
    }

    public static class ReduceClass extends Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text words, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String _value = "";
            String twoGram = "";
            String _key = "";
            ArrayList<String> threeGrams = new ArrayList<String>();

            for (Text value : values) {
                String[] splitWordsAndNumOfOccurrence = value.toString().split(" ");
                if (splitWordsAndNumOfOccurrence.length == 3) {
                    twoGram = value.toString();
                } else {
                    threeGrams.add(value.toString());
                }
            }

            for (String value : threeGrams) {
                String[] splitWordsAndNumOfOccurrence = value.split(" ");
                _key = String.format("%s %s %s", splitWordsAndNumOfOccurrence[0], splitWordsAndNumOfOccurrence[1], splitWordsAndNumOfOccurrence[2]);
                _value = String.format("%s %s", value.replace(_key + " ",""), twoGram);
                context.write(new Text(_key), new Text(_value));
            }
        }
    }

    public static class PartitionerClass extends Partitioner<Text, Text> {
        @Override
        public int getPartition(Text key, Text value, int numPartitions) {
            return Math.abs(key.hashCode()) % numPartitions;
        }
    }


    public static void main(String[] args) throws Exception {
        System.out.println("test print");
        System.out.println("args[0]: " + args[0]);
        System.out.println("args[1]: " + args[1]);
        System.out.println("args[2]: " + args[2]);

        Configuration conf = new Configuration();
        Job job = new Job(conf, "Join1");
        job.setJarByClass(Join1.class);

        job.setMapperClass(MapClass2Gram.class);

        job.setPartitionerClass(PartitionerClass.class);
        job.setReducerClass(ReduceClass.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(TextInputFormat.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}