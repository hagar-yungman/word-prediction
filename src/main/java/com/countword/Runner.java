package com.countword;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClientBuilder;
import com.amazonaws.services.elasticmapreduce.model.*;
import com.amazonaws.services.elasticmapreduce.util.StepFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Runner {
    public String jobFlowId;


    public Runner() throws ClassNotFoundException, IOException, InterruptedException {
        AmazonElasticMapReduce mapReduce = AmazonElasticMapReduceClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        ArrayList<StepConfig> steps = new ArrayList<StepConfig>();

         StepConfig enabledebugging = new StepConfig()
                .withName("Enable debugging")
                .withActionOnFailure("TERMINATE_JOB_FLOW")
                .withHadoopJarStep(new StepFactory().newEnableDebuggingStep());
        steps.add(enabledebugging);

    HadoopJarStepConfig hadoopWordCount1gram = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.WordCount")
                .withArgs("s3://datasets.elasticmapreduce/ngrams/books/20090715/heb-all/1gram/data", "s3n://noahagardsp/predict/1gram-output");

        StepConfig stepConfig1gram = new StepConfig()
                .withName("count 1 gram")
                .withHadoopJarStep(hadoopWordCount1gram)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfig1gram);

        HadoopJarStepConfig hadoopWordCount2gram = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.WordCount")
                .withArgs("s3://datasets.elasticmapreduce/ngrams/books/20090715/heb-all/2gram/data", "s3n://noahagardsp/predict/2gram-output");

        StepConfig stepConfig2gram = new StepConfig()
                .withName("count 2 gram")
                .withHadoopJarStep(hadoopWordCount2gram)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfig2gram);

        HadoopJarStepConfig hadoopWordCount3gram = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.WordCount")
                .withArgs("s3://datasets.elasticmapreduce/ngrams/books/20090715/heb-all/3gram/data", "s3n://noahagardsp/predict/3gram-output");

        StepConfig stepConfig3gram = new StepConfig()
                .withName("count 3 gram")
                .withHadoopJarStep(hadoopWordCount3gram)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfig3gram);

        HadoopJarStepConfig hadoopJoin1 = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.Join1")
                .withArgs("s3n://noahagardsp/predict/2gram-output/part-r-*", "s3n://noahagardsp/predict/3gram-output/part-r-*", "s3n://noahagardsp/predict/join1-output");

        StepConfig stepConfigjoin1 = new StepConfig()
                .withName("join 1")
                .withHadoopJarStep(hadoopJoin1)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfigjoin1);

        HadoopJarStepConfig hadoopJoin2 = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.Join2")
                .withArgs("s3n://noahagardsp/predict/2gram-output/part-r-*", "s3n://noahagardsp/predict/join1-output/part-r-*", "s3n://noahagardsp/predict/join2-output");

        StepConfig stepConfigjoin2 = new StepConfig()
                .withName("join 2")
                .withHadoopJarStep(hadoopJoin2)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfigjoin2);

        HadoopJarStepConfig hadoopProb2n = new HadoopJarStepConfig()
                .withJar("s3n://noahagardsp/predict/jar1/CountWord.jar")
                .withMainClass("com.countword.CalcProb2")
                .withArgs("s3n://noahagardsp/predict/join2-output/part-r-*", "s3n://noahagardsp/predict/prob-output", "s3n://noahagardsp/", "s3n://noahagardsp/predict/1gram-output");

        StepConfig stepConfigProb2n = new StepConfig()
                .withName("prob")
                .withHadoopJarStep(hadoopProb2n)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
        steps.add(stepConfigProb2n);

        JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
                .withInstanceCount(5)
                .withMasterInstanceType(InstanceType.M3Xlarge.toString())
                .withSlaveInstanceType(InstanceType.M3Xlarge.toString())
                .withHadoopVersion("2.7.2")
                .withKeepJobFlowAliveWhenNoSteps(false)
                .withPlacement(new PlacementType("us-east-1a"));


        RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
                .withName("ass2")
                .withReleaseLabel("emr-4.1.0")
                .withInstances(instances)

                .withSteps(steps)
                .withLogUri("s3n://noahagardsp/predict/logs/")
                .withJobFlowRole("EMR_EC2_DefaultRole")
                .withServiceRole("EMR_DefaultRole");

        RunJobFlowResult runJobFlowResult = mapReduce.runJobFlow(runFlowRequest);
        jobFlowId = runJobFlowResult.getJobFlowId();
    }

    public static void main(String [] args) throws ClassNotFoundException, IOException, InterruptedException
    {
        Runner services = new Runner();
        System.out.println("Ran job flow with id: " + services.jobFlowId);
    }
}
